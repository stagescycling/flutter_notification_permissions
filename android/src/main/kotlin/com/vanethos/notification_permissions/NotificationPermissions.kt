package com.vanethos.notification_permissions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.app.NotificationManagerCompat
import android.provider.Settings


class NotificationPermissions(private val applicationContext: Context, private var activity: Activity?) {

    fun setActivity(activity: Activity?) {
        this.activity = activity
    }

    fun getNotificationAccessStatus(): String {
        if (activity == null) {
            throw IllegalAccessException("Activity has not been set.")
        }

        val contentResolver = activity!!.contentResolver
        val enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners")
        val packageName: String = activity!!.packageName
        return if (enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName)) PERMISSION_GRANTED else PERMISSION_DENIED
    }

    fun requestNotificationAccess() {

        if (activity == null) {
            throw IllegalAccessException("Activity has not been set.")
        }

        activity?.let {

//            https://stackoverflow.com/questions/22663359/redirect-to-notification-access-settings
            val intent = Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS)
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, it.packageName)

            it.startActivity(intent)
        }
    }

    fun getNotificationPermissionStatus(): String {
        if (activity == null) {
            throw IllegalAccessException("Activity has not been set")
        }

        return if (NotificationManagerCompat.from(activity!!.applicationContext)
                        .areNotificationsEnabled()) PERMISSION_GRANTED else PERMISSION_DENIED
    }

    fun requestNotificationPermissions() {

        if (activity == null) {
            throw IllegalAccessException("Activity has not been set.")
        }

        activity?.let {

            // https://stackoverflow.com/a/45192258
            val intent = Intent()


//            when {
//                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
//                    // ACTION_APP_NOTIFICATION_SETTINGS was introduced in API level 26 aka Android O
//                    intent.action = Settings.ACTION_APP_NOTIFICATION_SETTINGS
//                    intent.putExtra(Settings.EXTRA_APP_PACKAGE, it.packageName)
//                }
//                Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
//                    intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"
//                    intent.putExtra("app_package", it.packageName)
//                    intent.putExtra("app_uid", it.applicationInfo.uid)
//                }
//                else -> {
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    intent.addCategory(Intent.CATEGORY_DEFAULT)
                    intent.data = Uri.parse("package:" + it.packageName)
//                }
//            }

            it.startActivity(intent)
        }
    }

    companion object {
        const val PERMISSION_GRANTED = "granted"
        const val PERMISSION_DENIED = "denied"
    }

}