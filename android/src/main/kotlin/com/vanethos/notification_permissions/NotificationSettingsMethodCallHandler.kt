package com.vanethos.notification_permissions

import android.util.Log
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class NotificationPermissionsMethodCallHandler(private val notificationPermissions: NotificationPermissions) :
        MethodChannel.MethodCallHandler {
//    private val TAG = "NotifPrmHndlr"
    private val TAG = "handlr"
    private var channel: MethodChannel? = null

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        try {
            when(call.method) {
                "getNotificationPermissionStatus" -> {
                    val status = notificationPermissions.getNotificationPermissionStatus()
                    result.success(status)
                }

                "requestNotificationPermissions" -> {

                    var status = notificationPermissions.getNotificationPermissionStatus()
                    if (status == NotificationPermissions.PERMISSION_DENIED) {
                        notificationPermissions.requestNotificationPermissions()
                        status = notificationPermissions.getNotificationPermissionStatus()
                    }
                    result.success(status)
                }

                "getNotificationAccessStatus" -> {
                    val status = notificationPermissions.getNotificationAccessStatus()
                    result.success(status)
                }

                "requestNotificationAccess" -> {

                    var status = notificationPermissions.getNotificationAccessStatus()
                    if (notificationPermissions.getNotificationAccessStatus() == NotificationPermissions.PERMISSION_DENIED) {
                        notificationPermissions.requestNotificationAccess()
                        status = notificationPermissions.getNotificationAccessStatus()
                    }
                    result.success(status)
                }
                else -> {
                    result.notImplemented()
                }
            }
        } catch (ex: Exception) {
            result.error(call.method, ex.message, null)
        }

    }

    /**
     * Registers this instance as a method call handler on the given `messenger`.
     *
     *
     * Stops any previously started and unstopped calls.
     *
     *
     * This should be cleaned with [.stopListening] once the messenger is disposed of.
     */
    fun startListening(messenger: BinaryMessenger) {
        if (channel != null) {
            Log.wtf(TAG, "Setting a method call handler before the last was disposed.")
            stopListening()
        }
        channel = MethodChannel(messenger, METHOD_CHANNEL)
        channel!!.setMethodCallHandler(this)
    }

    /**
     * Clears this instance from listening to method calls.
     *
     *
     * Does nothing if [.startListening] hasn't been called, or if we're already stopped.
     */
    fun stopListening() {
        if (channel == null) {
            Log.d(TAG,
                    "Tried to stop listening when no MethodChannel had been initialized.")
            return
        }
        channel!!.setMethodCallHandler(null)
        channel = null
    }

    companion object {
        private const val METHOD_CHANNEL = "notification_permissions"
    }

}