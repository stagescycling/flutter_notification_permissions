package com.vanethos.notification_permissions

import android.util.Log
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.FlutterPlugin.FlutterPluginBinding
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
//import io.flutter.plugin.common.PluginRegistry.Registrar


class NotificationPermissionsPlugin : FlutterPlugin, ActivityAware {
    private var methodCallHandler: NotificationPermissionsMethodCallHandler? = null
    private var notificationPermissions: NotificationPermissions? = null


    override fun onAttachedToEngine(binding: FlutterPluginBinding) {
        notificationPermissions = NotificationPermissions(binding.applicationContext,  /*activity=*/null)
        methodCallHandler = NotificationPermissionsMethodCallHandler(notificationPermissions!!)
        methodCallHandler!!.startListening(binding.binaryMessenger)
    }

    override fun onDetachedFromEngine(binding: FlutterPluginBinding) {
        if (methodCallHandler == null) {
            Log.wtf(TAG, "Already detached from the engine.")
            return
        }
        methodCallHandler!!.stopListening()
        methodCallHandler = null
        notificationPermissions = null
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        if (notificationPermissions == null) {
            Log.wtf(TAG, "notificationPermissions was never set.")
            return
        }
        notificationPermissions!!.setActivity(binding.activity)
    }

    override fun onDetachedFromActivity() {
        if (notificationPermissions == null) {
            Log.wtf(TAG, "notificationPermissions was never set.")
            return
        }
        notificationPermissions!!.setActivity(null)
    }

    override fun onDetachedFromActivityForConfigChanges() {
        onDetachedFromActivity()
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        onAttachedToActivity(binding)
    }

    companion object {
        private const val TAG = "NotificationPermissionsPlugin"

//        /**
//         * Registers a plugin implementation that uses the stable `io.flutter.plugin.common`
//         * package.
//         *
//         *
//         * Calling this automatically initializes the plugin. However plugins initialized this way
//         * won't react to changes in activity or context, unlike [NotificationPermissionsPlugin].
//         */
//        @JvmStatic
//        fun registerWith(registrar: Registrar) {
//            val handler = NotificationPermissionsMethodCallHandler(
//                    NotificationPermissions(registrar.context(), registrar.activity()))
//            handler.startListening(registrar.messenger())
//        }
    }
}